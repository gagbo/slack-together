DO $$ BEGIN
 CREATE TYPE "public"."work_location" AS ENUM('home', 'office', 'unknown');
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "location_schedules" (
	"id" serial PRIMARY KEY NOT NULL,
	"user_id" integer,
	"date" date NOT NULL,
	"location" "work_location"
);
--> statement-breakpoint
CREATE TABLE IF NOT EXISTS "users" (
	"id" serial PRIMARY KEY NOT NULL,
	"slack_user_id" text NOT NULL,
	"slack_team_id" text NOT NULL
);
--> statement-breakpoint
DO $$ BEGIN
 ALTER TABLE "location_schedules" ADD CONSTRAINT "location_schedules_user_id_users_id_fk" FOREIGN KEY ("user_id") REFERENCES "public"."users"("id") ON DELETE no action ON UPDATE no action;
EXCEPTION
 WHEN duplicate_object THEN null;
END $$;
--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "date_idx" ON "location_schedules" USING btree ("date" DESC NULLS LAST);--> statement-breakpoint
CREATE INDEX IF NOT EXISTS "slack_user_id_idx" ON "users" USING btree ("slack_user_id");