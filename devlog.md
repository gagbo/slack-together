# Devlog

## Objective

Make a small Slack app that
- has an App Home where you set your work location for the current week
  + In Office
  + At Home
  + Don’t know
- Changes current Slack status according to the choice made
- Keeps a list of people who are in the Office in the App Home

## Commands to do before the project is in "start working state"

```console
npm create cloudflare@latest -- --template=git@github.com:fiberplane/honc-template.git
# layout node in .envrc
nvim .envrc
# v22 in .nvmrc
nvim .nvmrc
npm i @fiberplane/hono@beta
npm i -D typescript-language-server
npm i -D typescript
neonctl branches create --name together --project-id tight-snow-53890643
neonctl set-context --branch together --project-id tight-snow-53890643
echo "DATABASE_URL=$(neonctl connection-string)" >> .dev.vars
# Make the edits to add the middleware and change port
nvim src/index.ts
# Edit ngrok config to add the tunnel
nvim ~/Library/Application\ Support/ngrok/ngrok.yml
npm install --save-dev --save-exact prettier
npm install --save-dev eslint-config-prettier
npm init @eslint/config@latest
# Add the eslint-config-prettier plugin
nvim eslint.config.mjs
```

Had to change the typescript-eslint package to `"typescript-eslint": "^8.0.0-alpha.10",` in order to get support for eslint 9:
https://github.com/typescript-eslint/typescript-eslint/issues/8211


## Websocket mode

The best Slack app development method is to use the new ["Socket mode"](https://api.slack.com/apis/socket-mode), since we do not have to setup a tunnel with ngrok or other.
But it seems `fpx` doesn’t support it. So building a Slack app "the easy way" isn’t really supported today.

## Issues

### Requestor "detected routes" do not update

As I edit my app to add the first Slack specific routes, Wrangler output tells me that indeed the app has been updated, but in FPX studio I still only see the default/template routes. Are routes only detected on startup? Or do I need to put the middleware literally everywhere?

It updates when we receive queries

### Would like a 'resend' button on failed payloads

### Console logs don’t appear

Neither `console.log` nor `console.info`

### Make a quick issue search

Some errors don’t appear in logs, but would be nice to search from there. For example, why is my json payload not deserialized correctly?

### Multi-arguments console logs aren’t displayed correctly

### This

```
✘ [ERROR] Uncaught (in promise) RangeError: Invalid string length

      at console.<computed>
  (file:///Users/gagbo/ghorg/gagbo/gitlab/together/node_modules/@fiberplane/hono/dist/honoMiddleware.js:90:32)
      at globalThis.fetch
  (file:///Users/gagbo/ghorg/gagbo/gitlab/together/node_modules/@fiberplane/hono/dist/replace-fetch.js:29:17)
```

```
                originalFetch(endpoint, {
                    method: "POST",
                    headers,
                    body: JSON.stringify(payload),
                }));
```

### Overview in requests needs a hint for the fetch target
