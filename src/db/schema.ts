import { pgTable, serial, text, pgEnum, index, integer, date } from 'drizzle-orm/pg-core';

export const users = pgTable('users', {
  id: serial('id').primaryKey(),
  slack_user_id: text('slack_user_id').notNull(),
}, (table) => {
  return {
    userIdx: index('slack_user_id_idx').on(table.slack_user_id)
  }
}
);

export const locationEnum = pgEnum('work_location', ['home', 'office', 'unknown'])

export const schedules = pgTable('location_schedules', {
  id: serial('id').primaryKey(),
  userId: integer('user_id').references(() => users.id),
  // TODO: formalize the format so that it contains only an ISO Date
  date: date('date', { mode: "string" }).notNull(),
  location: locationEnum('location'),
}, (table) => {
  return {
    dateIdx: index('date_idx').on(table.date.desc())
  }
});
