import { Hono } from "hono";
import { createHonoMiddleware } from "@fiberplane/hono";
import { neon } from "@neondatabase/serverless";
import { drizzle } from "drizzle-orm/neon-http";
import { users } from "./db/schema";
import v1 from "./routes/v1";

type Bindings = {
  DATABASE_URL: string;
  SLACK_SIGNING_SECRET: string;
  SLACK_BOT_TOKEN: string;
  // This is useful for WebSocket mode, but websocket mode
  // prevents using fpx
  APP_TOKEN: string;
};


const app = new Hono<{ Bindings: Bindings }>();

app.use(createHonoMiddleware(app));

app.route("/together/v1", v1);

app.get("/", (c) => {
  return c.text("Hello Hono!");
});

app.get("/api/users", async (c) => {
  const sql = neon(c.env.DATABASE_URL);
  const db = drizzle(sql);

  return c.json({
    users: await db.select().from(users),
  });
});

export default app;
