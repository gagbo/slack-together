import { HomeView, SectionBlock } from "@slack/types";
import { NeonHttpDatabase } from "drizzle-orm/neon-http";
import { schedules, users } from "../db/schema";
import { eq } from "drizzle-orm";

type Option = {
    text: {
        "type": "plain_text",
        text: string,
        emoji: boolean,
    },
    value: "home" | "office" | "unknown"
}

export async function buildHomeTab(db: NeonHttpDatabase<Record<string, never>>, user_id: string): Promise<HomeView> {

    let userList = await db.select().from(users).where(eq(users.slack_user_id, user_id));
    if (userList.length === 0) {
        await db.insert(users).values({ slack_user_id: user_id })
    }
    const user = (await db.select().from(users).where(eq(users.slack_user_id, user_id)))[0];

    const selfSchedules = await db.select().from(schedules).where(eq(schedules.userId, user.id))
    const now = normalizeDate(new Date())

    const options: { [key: string]: Option } = {
        home: {
            "text": {
                "type": "plain_text",
                "text": "*Home*",
                "emoji": true
            },
            "value": "home"
        },
        office: {
            "text": {
                "type": "plain_text",
                "text": "*Office*",
                "emoji": true
            },
            "value": "office"
        },

        undecided: {
            "text": {
                "type": "plain_text",
                "text": "*Undecided*",
                "emoji": true
            },
            "value": "unknown"
        }
    }

    const date_elements: SectionBlock[] = currentWeek().map((day) => {
        const focusOnLoad = day.getDay() === now.getDay()
        const storedValue = selfSchedules.find((schedule) => schedule.date === now.toDateString())?.location ?? "unknown";
        const defaultOption = options[storedValue];
        return {
            type: "section",
            text: {
                type: "mrkdwn",
                text: `<!date^${day.getTime() / 1000}^{date_long_pretty}|${day.toDateString()}>`
            },
            accessory: {
                type: "static_select",
                placeholder: {
                    type: "plain_text",
                    text: "Work location",
                    emoji: true
                },
                focus_on_load: focusOnLoad,
                options: [
                    ...Object.values(options)
                ],
                initial_option: defaultOption,
                action_id: `location-action_${day.toDateString()}`
            }
        }
    });

    const blocks: SectionBlock[] = [
        {
            type: "section",
            text: {
                type: "mrkdwn",
                text: "Get together"
            },
            accessory: {
                type: "image",
                image_url: "https://api.slack.com/img/blocks/bkb_template_images/notifications.png",
                alt_text: "calendar thumbnail"
            }
        },
        ...date_elements
    ]

    return {
        "type": "home",
        "blocks": blocks
    }
}

type Weekday = "Sunday" | "Monday" | "Tuesday" | "Wednesday" | "Thursday" | "Friday" | "Saturday";

function dateToWeekday(date: Date): Weekday {
    const weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    return weekday[date.getDay()];
}

function normalizeDate(date: Date): Date {

    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date
}

function currentWeek(): Date[] {
    const previousSunday = normalizeDate(new Date())
    previousSunday.setDate(previousSunday.getDate() - previousSunday.getDay())

    let dates: Date[] = []
    for (let i = 1; i <= 5; i++) {
        const day = new Date(previousSunday.getTime());
        day.setDate(day.getDate() + i);
        dates.push(day);
    }
    return dates
}
