import { SlackAction } from "@slack/bolt";
import { Hono } from "hono";
import { WebClient as SlackClient } from "@slack/web-api";
import { buildHomeTab } from "../../slack/home-tab";
import { HTTPException } from "hono/http-exception";
import { drizzle } from "drizzle-orm/neon-http";
import { neon } from "@neondatabase/serverless";
import { schedules, users } from "../../db/schema";
import { eq } from "drizzle-orm";

type Bindings = {
    DATABASE_URL: string;
    SLACK_SIGNING_SECRET: string;
    SLACK_BOT_TOKEN: string;
    // This is useful for WebSocket mode, but websocket mode
    // prevents using fpx
    APP_TOKEN: string;
};


const app = new Hono<{ Bindings: Bindings }>();

app.post("/", async (c) => {

    const str_event = await c.req.parseBody<{ payload: string }>();

    const event = JSON.parse(str_event.payload) as SlackAction;


    // Handle App Home Opened event only
    if (event.type === "block_actions") {
        console.info("Handling Block Interaction event", JSON.stringify(event, null, 2));
        const sql = neon(c.env.DATABASE_URL);
        const db = drizzle(sql);

        const userId = event.user.id;

        for (let action of event.actions) {
            console.log(`action: ${action}`);
            const actionId = action.action_id
            if (action.type === "static_select") {
                const option = action.selected_option.value
                const date = actionId.split('_')[1];

                let userList = await db.select().from(users).where(eq(users.slack_user_id, userId));
                if (userList.length === 0) {
                    await db.insert(users).values({ slack_user_id: userId })
                }
                const user = (await db.select().from(users).where(eq(users.slack_user_id, userId)))[0];
                // TODO: Make this an update
                const scheduleElement = {
                    date,
                    location: option,
                    userId: user.id,
                }
                console.log(`Inserting ${JSON.stringify(scheduleElement)}`)
                await db.insert(schedules).values(scheduleElement)
            }
        }

        // Force redraw of Home Tab
        const client = new SlackClient(c.env.SLACK_BOT_TOKEN);
        const result = await client.views.publish({
            user_id: userId,
            view: await buildHomeTab(db, userId),
        });
        console.info("Slack API call", result);
        return c.text("Handled event");
    }

    console.error("Dealing with event failed: ", JSON.stringify(event, null, 2))
    throw new HTTPException(400, { message: 'The application cannot deal with this event type' })
});

export default app;
