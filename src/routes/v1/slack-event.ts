import { EnvelopedEvent, SlackEvent } from "@slack/bolt";
import { Hono } from "hono";
import { WebClient as SlackClient } from "@slack/web-api";
import { buildHomeTab } from "../../slack/home-tab";
import { HTTPException } from "hono/http-exception";
import { drizzle } from "drizzle-orm/neon-http";
import { neon } from "@neondatabase/serverless";

type Bindings = {
    DATABASE_URL: string;
    SLACK_SIGNING_SECRET: string;
    SLACK_BOT_TOKEN: string;
    // This is useful for WebSocket mode, but websocket mode
    // prevents using fpx
    APP_TOKEN: string;
};


const app = new Hono<{ Bindings: Bindings }>();

app.post("/", async (c) => {

    const event = await c.req.json<EnvelopedEvent<SlackEvent>>();

    // https://github.com/slackapi/bolt-js/blob/b12db71bf82eacf1fa8135eb56261005a3706c68/src/types/events/base-events.ts#L1073
    // @ts-ignore
    if (event.type === "url_verification") {
        // @ts-ignore
        return c.text(event.challenge);
    }

    // Handle App Home Opened event only
    if (event.event.type === "app_home_opened") {
        console.info("Handling App Home Opened");

        const sql = neon(c.env.DATABASE_URL);
        const db = drizzle(sql);

        const user_id = event.event.user;

        const client = new SlackClient(c.env.SLACK_BOT_TOKEN);
        const result = await client.views.publish({
            user_id,
            view: await buildHomeTab(db, user_id),
        });
        console.info("Slack API call", result);
        return c.text("Handled event");
    }

    console.error("Dealing with event failed: ", JSON.stringify(event, null, 2))
    throw new HTTPException(400, { message: 'The application cannot deal with this event type' })
});

export default app;
