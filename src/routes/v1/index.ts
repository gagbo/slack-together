import slackEventHandler from "./slack-event";
import slackInteractionHandler from "./slack-interaction";

import { Hono } from "hono";


const app = new Hono();

app.route("/slack-event", slackEventHandler);
app.route("/slack-interaction", slackInteractionHandler);

export default app;
